from time import time
import numpy as np
import graph_cut
import sys


def compute_energy(labels, unary, vertC, horC, metric):
    n, m, k = unary.shape
    row, col = np.indices((n, m))
    energy = np.sum(unary[row, col, labels])
    energy += np.sum(metric[labels[:, 1:], labels[:, :-1]] * horC)
    energy += np.sum(metric[labels[1:, :], labels[:-1, :]] * vertC)
    return energy


def alphaExpansionGridPotts(unary, vertC, horC, metric, maxIter=500,
                            display=False, numStart=1, randOrder=False):
    INF = 1e9

    n, m, k = unary.shape
    if np.min(unary) < 0:
        unary += np.min(unary)

    def merge_nums(i, j):
        return m * i + j

    def split_nums(x):
        return x // m, x % m

    term_weights = np.zeros((n*m, 2))
    alphas = np.arange(k)
    energy_opt = np.inf

    for start_num in range(numStart):
        labels = np.random.randint(0, k, (n, m))
        if randOrder:
            np.random.shuffle(alphas)
        if display:
            print("Start number #{}".format(start_num))
            sys.stdout.flush()

        tm = []
        old_energy = np.inf
        for iter in range(maxIter):
            if display:
                print("Global iteration #{}".format(iter))
                sys.stdout.flush()

            old_labels = labels.copy()
            cur_tm = time()
            for j, alpha in enumerate(alphas):
                term_weights.fill(0)

                row, col = np.indices((n, m))
                term_weights[np.arange(n*m).reshape(n, m), 1] = unary[row, col, labels]
                term_weights[np.arange(n*m).reshape(n, m), 0] = unary[row, col, alpha]

                indices = list(map(lambda x: merge_nums(*x), zip(*np.where(labels == alpha))))
                term_weights[indices, 1] = INF

                hor_edges = (np.arange(2*n*m) // 2)[1:-1].reshape(-1, 2)[(np.arange(n*m-1) % m != (m - 1))]
                ver_edges = np.vstack((np.arange((n-1)*m), np.arange((n-1)*m) + m)).T

                def get_pots(x, arr):
                    i, j = split_nums(x[0])
                    e, q = split_nums(x[1])
                    beta = labels[i, j]
                    gamma = labels[e, q]
                    phi_00 = arr[i, j] * metric[beta, gamma]
                    phi_01 = arr[i, j] * metric[beta, alpha]
                    phi_10 = arr[i, j] * metric[alpha, gamma]
                    phi_11 = arr[i, j] * metric[alpha, alpha]
                    theta_i = term_weights[x[0]]
                    theta_j = term_weights[x[1]]
                    phi_01 -= phi_00
                    # beware of index swap!
                    theta_i[1] += phi_00
                    phi_10 -= phi_11
                    theta_i[0] += phi_11
                    theta_i[1] += (phi_01 - phi_10) / 2.0
                    theta_j[1] -= (phi_01 - phi_10) / 2.0
                    term_weights[x[0]] = theta_i
                    term_weights[x[1]] = theta_j
                    return (phi_01 + phi_10) / 2.0

                hor_pots = np.array(list(map(lambda x: get_pots(x, horC), hor_edges)))[:,np.newaxis]
                ver_pots = np.array(list(map(lambda y: get_pots(y, vertC), ver_edges)))[:,np.newaxis]

                edge_weights = np.vstack((np.hstack((hor_edges, hor_pots, hor_pots)),
                                          np.hstack((ver_edges, ver_pots, ver_pots))
                               ))
                (cut, alpha_labels) = graph_cut.graph_cut(term_weights, edge_weights)

                labels[alpha_labels.reshape(n, m) == 1] = alpha
                energy = compute_energy(labels, unary, vertC, horC, metric)
              #  assert energy <= old_energy
                old_energy = energy
                if display:
                    print("Iteration #{}:\n    label: {}\n    energy: {}".format(j, alpha, energy))
                    sys.stdout.flush()
            cur_tm = time() - cur_tm
            tm.append(cur_tm)
            if np.allclose(labels, old_labels):
                break
        if energy < energy_opt:
            energy_opt = energy
            tm_opt = tm
            labels_opt = labels
    return labels_opt, energy_opt, tm_opt


def stichImages(images, seeds):
    INF = 1e9

    imgs = np.array(images)
    k, n, m, _ = imgs.shape
    sigma = 0.00002
    horC = np.mean(np.exp(-(np.sum(np.diff(imgs, axis=2) ** 2, axis=-1)) ** 2 / (2 * sigma ** 2)), axis=0)
    vertC = np.mean(np.exp(-(np.sum(np.diff(imgs, axis=1) ** 2, axis=-1)) ** 2 / (2 * sigma ** 2)), axis=0)
    metric = np.ones((k, k)) - np.eye(k)
    unary = np.zeros((n, m, k))
    for i in range(k):
        indices = np.where(seeds[i][:, :, np.newaxis])
        for j in np.delete(np.arange(k), i):
            ind_j = (indices[0], indices[1], indices[2] + j)
            unary[ind_j] = INF
    labels, energy, time = alphaExpansionGridPotts(unary, vertC, horC, metric, numStart=3, randOrder=True)
    result_img = np.zeros(labels.shape + (3,))
    for i in range(k):
        result_img += imgs[i] * (labels == i)[:,:,np.newaxis]
    return result_img, labels
